#App1
module "app1_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "App1"
  cidr = "10.10.1.0/24"

  azs             = ["${var.region}a", "${var.region}b",]
  private_subnets = ["10.10.1.0/26", "10.10.1.64/26",]
  public_subnets  = ["10.10.1.224/28", "10.10.1.240/28",]
}

module "app1_spoke" {
  source  = "terraform-aviatrix-modules/mc-spoke/aviatrix"
  version = "1.1.2"

  cloud            = "AWS"
  name             = "App1"
  region           = var.region
  account          = "AWS"
  transit_gw       = lookup(local.transit_map, var.region, null)

  use_existing_vpc = true
  vpc_id           = module.app1_vpc.vpc_id
  gw_subnet        = module.app1_vpc.public_subnets_cidr_blocks[0]
  hagw_subnet      = module.app1_vpc.public_subnets_cidr_blocks[1]
}