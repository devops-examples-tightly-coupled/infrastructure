variable "region" {
    default = "eu-central-1"
}

locals {
  transit_map = data.tfe_outputs.core-network.values.transit_map
}